package com.springapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.UNPROCESSABLE_ENTITY)
public class InputValidation extends RuntimeException {

	public InputValidation(String exceptionMessage) {
		super(exceptionMessage);
	}
	
}
