package com.springapplication.dao;

import java.util.List;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.springapplication.dto.Employee;

@Component
public class ApplicationDao {
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Transactional
	public Employee getEmployee(int empId) {
		LOGGER.info("This is getEmployee Dao");
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();
		Employee employee = session.get(Employee.class, empId);
		session.getTransaction().commit();
		session.close();
		return employee;
	}

	@Transactional
	public void createEmployee(Employee employee) {
		LOGGER.info("This is createEmployee Dao");
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();
		session.save(employee);
		session.getTransaction().commit();
		session.close();
	}
	
	@Transactional
	public List<Employee> getEmployeeList(){
		LOGGER.info("This is getEmployeeList Dao");
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();
		Query query = session.createQuery("from Employee");
		List<Employee> employees = query.getResultList();
		session.getTransaction().commit();
		session.close();
		return employees;
	}
	

}
