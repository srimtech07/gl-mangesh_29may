package com.springapplication.controller;

import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.springapplication.dto.Department;
import com.springapplication.dto.Employee;
import com.springapplication.exception.InputValidation;
import com.springapplication.service.ApplicationService;


@RestController
public class ApplicationController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ApplicationService applicationService;
	
	@RequestMapping(path = "/getEmployee/{empId}", method = RequestMethod.GET)
	public Employee getEmployee(@PathVariable("empId") int empId) {
		LOGGER.info("This is getEmployee Controller");
		return applicationService.getEmployee(empId);
	}

	@RequestMapping(path = "/create", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	public void createEmployee(@RequestBody Employee employee) {
		LOGGER.info("This is createEmployee Controller");
		Pattern pattern = Pattern.compile("[a-zA-Z0-9]*");
		Matcher matcher = pattern.matcher(employee.getEmpName());
		if(!matcher.matches()) {
			throw new InputValidation("Employee Name contains special character(s)");
		}
		applicationService.createEmployee(employee);
	}

	@RequestMapping(path = "/getAll", method = RequestMethod.GET)
	public List<Employee> getEmployeeList() {
		LOGGER.info("This is getEmployeeList Controller");
		List<Employee> list = applicationService.getEmployeeList();
		list.sort(Comparator.comparing(Employee::getEmpName).thenComparing(Employee::getSalary));
		return list;
	}
}
