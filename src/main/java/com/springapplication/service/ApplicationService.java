package com.springapplication.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springapplication.dao.ApplicationDao;
import com.springapplication.dto.Employee;

@Service
public class ApplicationService {
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ApplicationDao applicationDao;

	public Employee getEmployee(int empId) {
		LOGGER.info("This is getEmployee service");
		return applicationDao.getEmployee(empId);
	}

	public void createEmployee(Employee employee) {
		LOGGER.info("This is createEmployee service");
		applicationDao.createEmployee(employee);
	}

	public List<Employee> getEmployeeList() {
		LOGGER.info("This is getEmployeeList service");
		return applicationDao.getEmployeeList();
	}
}
